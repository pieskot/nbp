import json
import urllib.request
from datetime import date, timedelta, datetime
import time


class NBP():
    def __init__(self):
        self.oz = 31.1035
        self.url_eur = 'http://api.nbp.pl/api/exchangerates/rates/c/eur/last//?format=json'
        self.url_sek = 'http://api.nbp.pl/api/exchangerates/rates/c/sek/last//?format=json'
        self.url_gold = 'http://api.nbp.pl/api/cenyzlota/?format=json'

    def get_gold(self):
        with urllib.request.urlopen(self.url_gold) as url:
            web_data = json.loads(url.read().decode())
        self.gold_date = web_data[0]['data']
        self.gold_price = round(web_data[0]['cena'] * self.oz, 2)

    def get_sek(self):
        with urllib.request.urlopen(self.url_sek) as url:
            web_data = json.loads(url.read().decode())
        self.sek_bid = web_data['rates'][0]['bid']
        self.sek_ask = web_data['rates'][0]['ask']
        self.sek_date = web_data['rates'][0]['effectiveDate']

    def get_eur(self):
        with urllib.request.urlopen(self.url_eur) as url:
            web_data = json.loads(url.read().decode())
        self.eur_bid = web_data['rates'][0]['bid']
        self.eur_ask = web_data['rates'][0]['ask']
        self.eur_date = web_data['rates'][0]['effectiveDate']


nbp = NBP()
while True:
    print(datetime.now())
    print()
    try:
        nbp.get_gold()
        nbp.get_sek()
        nbp.get_eur()
        print('1oz - ' + str(nbp.gold_price) + ' pln  (' + str(nbp.gold_date) + ')')
        print('SEK - ask: ' + str(nbp.sek_ask) + '  bid: ' + str(nbp.sek_bid))
        print('EUR - ask: ' + str(nbp.eur_ask) + '  bid: ' + str(nbp.eur_bid))
        sek_eur = round(1 * nbp.eur_ask / nbp.sek_bid, 2)
        print('sek/eur: ' + str(sek_eur))
    except Exception as e:
        print(e)
    print()
    time.sleep(3600)
